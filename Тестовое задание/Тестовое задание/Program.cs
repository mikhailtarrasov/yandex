﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using SQLitePCL;
using Тестовое_задание.Models;

namespace Тестовое_задание
{
    public struct MonthOfYear
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var databaseName = "database";
            
            if (args.Length > 0)
            {
                if (File.Exists(args[0]))
                {
                    if (File.Exists(databaseName))
                        File.Delete(databaseName);

                    var dbClient = new DbClient(databaseName);
                    var filePath = args[0];
                    try
                    {
                        dbClient.FillInDatabase(filePath);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Произошла ошибка при чтении файла, попробуйте указать другой файл!");
                    }
                    Menu(dbClient);
                    Console.WriteLine("Нажмите любую клавишу...");
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Ошибка! Файла с указанным именем не существует, либо имя введено некорректно!");
                }
            }
            else
            {
                Console.WriteLine("Ошибка! Передайте название файла вторым параметром!");
            }
            //var filePath = "orders.txt";
            //var productCount = 7;
            //var currYear = DateTime.Now.Year;
            //var currMonth = DateTime.Now.Month;
            //var numberMonthsAgo = 2;
            //var startDate = new DateTime(currMonth - numberMonthsAgo < 1 ? currYear - 1 : currYear, currMonth - numberMonthsAgo < 1 ? 12 + currMonth - numberMonthsAgo : currMonth - numberMonthsAgo, 1);
            //var endDate = new DateTime(currYear, currMonth, DateTime.Now.Day);
            //InitializeRandomOrdersFile(filePath, productCount, startDate, endDate
        }

        private static void Menu(DbClient dbClient)
        {
            var currentMonth = new MonthOfYear()
            {
                Year = DateTime.Now.Year,
                Month = DateTime.Now.Month
            };
            var previousMonth = new MonthOfYear()
            {
                Year = currentMonth.Month == 1 ? currentMonth.Year - 1 : currentMonth.Year,
                Month = currentMonth.Month == 1 ? 12 : currentMonth.Month - 1
            };

            PrintMenu();
            var menu = Console.ReadKey().KeyChar;
            Console.WriteLine("\n");
            if (menu != '0')
            {
                do
                {
                    Console.Clear();
                    List<Product> products;
                    switch (menu)
                    {
                        case '1':
                            Console.WriteLine("Отчёт по всем продуктам за текущей месяц:\n");
                            var info = dbClient.GetInfoEachProductForMonth(currentMonth);
                            foreach (var productInfo in info)
                                Console.WriteLine(productInfo.ProductName + "\t" + productInfo.OrdersCount + "\t" + productInfo.AmountOfOrders.ToString("F2"));
                            break;
                        case '2':
                            products = dbClient.GetProductsNotOrderedInTheSecondMonth(currentMonth, previousMonth);
                            if (products.Count > 0)
                            {
                                Console.WriteLine("Список продуктов, которые были заказаны в текущем месяце, но которых не было в предыдущем:");
                                PrintProductsNames(products);
                            }
                            else
                                Console.WriteLine("Все продукты, заказанные в текущем месяце, также были заказаны и в предыдущем.");
                            break;
                        case '3':
                            products = dbClient.GetProductsNotOrderedInTheSecondMonth(previousMonth, currentMonth);
                            if (products.Count > 0)
                            {
                                Console.WriteLine("Список продуктов, которые были только в прошлом месяце, но не в текущем:");
                                PrintProductsNames(products);
                            }
                            else
                                Console.WriteLine("Все продукты, заказанные в текущем месяце, также были заказаны и в предыдущем.");

                            products = dbClient.GetProductsNotOrderedInTheSecondMonth(currentMonth, previousMonth);
                            if (products.Count > 0)
                            {
                                Console.WriteLine("Список продуктов, которые были только в текущем месяце, но не в прошлом:");
                                PrintProductsNames(products);
                            }
                            else
                                Console.WriteLine("Все продукты, заказанные в текущем месяце, также были заказаны и в предыдущем.");
                            break;
                        case '4':
                            Console.WriteLine("Отчет по продуктам, которые принесли наибольшую прибыль по месяцам:\n");
                            Console.WriteLine("Период\tПродукт\tСумма\tДоля %");
                            var tempMonth = dbClient.GetMonthOfFirstOrder();
                            var lastMonth = dbClient.GetMonthOfLastOrder();
                            while (tempMonth <= lastMonth)
                            {
                                var monthlyReport = dbClient.GetInfoEachProductForMonth(new MonthOfYear() { Year = tempMonth.Year, Month = tempMonth.Month });

                                var topProduct = FindAProductWithTheHighestRevenue(monthlyReport);
                                Console.WriteLine(tempMonth.ToString("MMM yy") + "\t" + topProduct.ProductName + "\t" + topProduct.AmountOfOrders.ToString("0.00") + "\t" + (topProduct.ShareOfTotalAmount.Value * 100).ToString("0.00"));
                                tempMonth = tempMonth.AddMonths(1);
                            }
                            break;
                        case '0':
                            Console.WriteLine("Завершение работы...");
                            break;
                        default:
                            Console.WriteLine("Ошибка, попробуйте ещё раз!");
                            break;
                    }
                    PrintMenu();
                    menu = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n");
                } while (menu != '0');
            }
        }

        private static void PrintProductsNames(List<Product> products)
        {
            foreach (var product in products)
                Console.WriteLine(product.Name);
        }

        private static ProductSalesForTheMonth FindAProductWithTheHighestRevenue(List<ProductSalesForTheMonth> monthlyReport)
        {
            var maxAmountOfOrders = monthlyReport.Max(x => x.AmountOfOrders);
            var resultProduct = monthlyReport.FirstOrDefault(x => x.AmountOfOrders == maxAmountOfOrders);

            var totalAmount = 0d;
            foreach (var product in monthlyReport)
                totalAmount += product.AmountOfOrders;

            resultProduct.ShareOfTotalAmount = maxAmountOfOrders / totalAmount;

            return resultProduct;
        }

        private static void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("\tВыберите действие:\n");
            Console.WriteLine("1. Вывести количество и сумму заказов по каждому продукту за текущей месяц");
            Console.WriteLine(
                "2. Вывести все продукты, которые были заказаны в текущем месяце, но которых не было в прошлом");
            Console.WriteLine(
                "3. Вывести все продукты, которые были только в прошлом месяце, но не в текущем, а какие — только в текущем месяце, но не в прошлом");
            Console.WriteLine(
                "4. Помесячно вывести продукт, по которому была максимальная сумма заказов за этот период, сумму по этому продукту и его долю от общего объема за этот период");
            Console.WriteLine("0. Закончить работу");
            Console.Write("\nВаш выбор > ");
        }

        private static void InitializeRandomOrdersFile(string filePath, int productCount, DateTime startDate, DateTime endDate)
        {
            FileStream fileStream = null;
            try
            {
                fileStream = new FileStream(filePath, FileMode.Create);
                var streamWriter = new StreamWriter(fileStream);

                var rand = new Random();
                int maxOrdersPerDay = 12000;
                int orderId = 1;

                string[] columnNames = new string[] { "id", "dt", "amount", "product_id" };
                string columnNamesString = "";
                for (int i = 0; i < columnNames.Length; i++)
                {
                    columnNamesString += columnNames[i];
                    if (i != columnNames.Length - 1)
                        columnNamesString += "\t";
                }
                streamWriter.WriteLine(columnNamesString);

                for (var currentDay = startDate; currentDay < endDate; currentDay = currentDay.AddDays(1))
                {
                    var randOrdersCountPerDay = rand.Next(1, maxOrdersPerDay);
                    for (int orderNumber = 1; orderNumber < randOrdersCountPerDay; orderNumber++, orderId++)
                    {
                        string order = CreateOrderString(orderNumber, orderId, currentDay, productCount);
                        streamWriter.WriteLine(order);
                    }
                }
                streamWriter.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                if(fileStream != null)
                    fileStream.Dispose();
            }
        }

        private static string CreateOrderString(int orderNumberPerDay, int orderId, DateTime currentDay, int productCount)
        {
            var rand = new Random(orderNumberPerDay + orderId);
            
            var orderDate = currentDay.Date.ToShortDateString();
            var orderTime = currentDay.TimeOfDay + TimeSpan.FromHours(rand.Next(10, 19)) +
                            TimeSpan.FromMinutes(rand.Next(0, 60));             // по одному заказу каждый час с 10:00
            var orderAmount = (rand.NextDouble() * 250).ToString("F2");

            /*В текущем месяце нет заказов первого и второго продуктов, в предыдущем - последнего (7)*/
            var productId = currentDay.Month == DateTime.Now.Month ? rand.Next(3, productCount+1) : rand.Next(1, productCount);

            int errorStrings = 4;                                               // 4 ошибочных строки
            float errorStringsPercentage = 0.01f;                                   // Задаём процент выпадания ошибочных строк
            int maxNumberStr = (int)(errorStrings / (errorStringsPercentage / 100));       // Определяем максимальное рандомное число для заданного процента ошибок
            var randomNumber = rand.Next(1, maxNumberStr);

            string order;
            switch (randomNumber)
            {
                case 1:     // несовпадение типов даты
                    order = orderId + "\t" + "666" + "\t" + orderAmount + "\t" + productId;
                    break;
                case 2:     // не хватает суммы заказа
                    order = orderId + "\t" + orderDate + "T" + orderTime + "\t" + "\t" + productId;
                    break;
                case 3:     // не хватает даты
                    order = orderId + "\t" + "\t" + orderAmount + "\t" + productId;
                    break;
                case 4:     // несуществующий товар
                    productId += rand.Next(-1000, 1000);
                    order = orderId + "\t" + orderDate + "T" + orderTime + "\t" + orderAmount + "\t" + productId;
                    break;
                default:
                    order = orderId + "\t" + orderDate + "T" + orderTime + "\t" + orderAmount + "\t" + productId;
                    break;
            }
            return order;
        }
    }
}