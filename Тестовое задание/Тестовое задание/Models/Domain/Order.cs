﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Тестовое_задание.Models
{
    public class Order
    {
        [PrimaryKey]
        public int Id { get; set; }
        public DateTime Dt { get; set; }
        public int ProductId { get; set; }
        public float Amount { get; set; }
    }
}
