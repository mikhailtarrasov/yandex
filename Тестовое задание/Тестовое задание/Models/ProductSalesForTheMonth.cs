﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Тестовое_задание.Models
{
    public class ProductSalesForTheMonth
    {
        public string ProductName { get; set; }
        public int OrdersCount { get; set; }
        public double AmountOfOrders { get; set; }
        public double? ShareOfTotalAmount { get; set; }
    }
}
