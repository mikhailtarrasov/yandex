﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Тестовое_задание.Models;

namespace Тестовое_задание
{
    public class DbClient : IDisposable
    {
        private SQLiteConnection _conn;

        public DbClient(string databaseName)
        {
            _conn = new SQLiteConnection(databaseName);
        }

        public void Dispose()
        {
            if (_conn != null) _conn.Dispose();
        }

        public void FillInDatabase(string filePath)
        {
            FillInProductsInDatabase();
            FillInOrdersFromFileToDatabase(filePath);
        }

        public List<ProductSalesForTheMonth> GetInfoEachProductForMonth(MonthOfYear currentMonth)
        {
            var products = _conn.Table<Product>();

            var startCurrentMonth = new DateTime(currentMonth.Year, currentMonth.Month, 1);
            var endCurrentMonth = new DateTime(currentMonth.Year, currentMonth.Month, DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month));

            var sales = new List<ProductSalesForTheMonth>();
            for (int i = 0; i < products.Count(); i++)
            {
                int productId = i + 1;
                var product = products.FirstOrDefault(x => x.Id == productId);

                var orders = _conn.Table<Order>().OrderBy(x => x.Dt).Where(x => 
                    x.ProductId == productId &&
                    x.Dt >= startCurrentMonth && 
                    x.Dt <= endCurrentMonth).ToList();
                
                sales.Add(GetProductSalesForTheMonthByOrders(orders, product));
            }
            return sales;
        }

        public List<Product> GetProductsNotOrderedInTheSecondMonth(MonthOfYear existThisMonth, MonthOfYear doesntExistThisMonth)
        {
            var resultProducts = new List<Product>();
            var currentMonthProductsIds = GetProductsThatWereOrderedWithinAMonth(existThisMonth);

            var previousMonthProductsIds = GetProductsThatWereOrderedWithinAMonth(doesntExistThisMonth);

            foreach (var productId in currentMonthProductsIds)
                if (!previousMonthProductsIds.Contains(productId)) resultProducts.Add(_conn.Table<Product>().FirstOrDefault(x => x.Id == productId));

            return resultProducts;
        }

        public DateTime GetMonthOfFirstOrder()
        {
            var date = _conn.Table<Order>().Min(x => x.Dt).Date;
            return new DateTime(date.Year, date.Month, 1);
        }

        public DateTime GetMonthOfLastOrder()
        {
            var date =_conn.Table<Order>().Max(x => x.Dt).Date;
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        private void FillInProductsInDatabase()
        {
            _conn.CreateTable<Product>();
            for (int i = 0; i < 7; i++)
            {
                _conn.Insert(new Product()
                {
                    Name = ((char)(65 + i)).ToString()
                });
            }
        }

        private void FillInOrdersFromFileToDatabase(string filePath)
        {
            FileStream fileStream = null;
            try
            {
                _conn.CreateTable<Order>();
                fileStream = new FileStream(filePath, FileMode.Open);
                var streamReader = new StreamReader(fileStream);
                string[] columnNames;

                if (!streamReader.EndOfStream)
                    columnNames = streamReader.ReadLine().Split('	');
                else
                    columnNames = null;

                if (columnNames != null)
                {
                    _conn.RunInTransaction(() =>
                    {
                        while (!streamReader.EndOfStream)
                        {
                            var newOrder = new Order();

                            var splitString = streamReader.ReadLine().Split('	');

                            if (ReportIfOrderIdIncorrect(splitString[0]))
                                continue;
                            newOrder.Id = GetOrderId(splitString[0]);

                            if (ReportIfEmpty(newOrder.Id, splitString[1], columnNames[1]) ||                   // Проверка даты на пустоту
                                ReportIfDateIncorrect(newOrder.Id, splitString[1], columnNames[1]) ||           // и на корректность
                                ReportIfEmpty(newOrder.Id, splitString[2], columnNames[2]) ||                   // Проверка суммы на пустоту
                                ReportIfAmountIncorrect(newOrder.Id, splitString[2], columnNames[2]) ||         // и на корректность
                                ReportIfEmpty(newOrder.Id, splitString[3], columnNames[3]) ||                   // Проверка идентификатора продукта на пустоту
                                ReportIfProductIdIncorrect(_conn, newOrder.Id, splitString[3], columnNames[3])) // и на наличие в таблице продуктов
                                continue;

                            newOrder.Dt = GetDateTimeFromString(splitString[1]);
                            newOrder.Amount = GetAmountFromString(splitString[2]);
                            newOrder.ProductId = GetProductId(splitString[3]);

                            _conn.Insert(newOrder);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Dispose();
            }
        }

        private ProductSalesForTheMonth GetProductSalesForTheMonthByOrders(List<Order> orders, Product product)
        {
            var sales = new ProductSalesForTheMonth();
            sales.ProductName = product.Name;
            sales.OrdersCount = orders.Count;
            sales.AmountOfOrders = 0f;
            if (orders.Any())
                foreach (var order in orders)
                    sales.AmountOfOrders += order.Amount;
            return sales;
        }

        private int[] GetProductsThatWereOrderedWithinAMonth(MonthOfYear date)
        {
            var startMonth = new DateTime(date.Year, date.Month, 1);
            var endMonth = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            return _conn.Table<Order>()
                .Where(x => x.Dt >= startMonth && x.Dt <= endMonth).ToArray()
                .Select(x => x.ProductId)
                .Distinct().OrderBy(x => x).ToArray();
        }

        private bool ReportIfOrderIdIncorrect(string orderIdStr)
        {
            int id;
            if (!Int32.TryParse(orderIdStr, out id))
            {
                Console.WriteLine("Ошибка! Проблема с идентификатором - \"{0}\"!", orderIdStr);
                return true;
            }
            return false;
        }

        private int GetOrderId(string orderIdStr)
        {
            return Int32.Parse(orderIdStr);
        }

        private bool ReportIfProductIdIncorrect(SQLiteConnection db, int orderId, string productIdStr, string columnName)
        {
            bool result = true;

            int productId;
            if (Int32.TryParse(productIdStr, out productId))
            {
                var dbProduct = db.Table<Product>().FirstOrDefault(x => x.Id.Equals(productId));

                if (dbProduct == null)
                    Console.WriteLine(
                        "Ошибка! Заказ - {0}. Параметра {1} с идентификатором - \"{2}\" нет в таблице продуктов!",
                        orderId, columnName, productIdStr);
                else result = false;
            }
            else
                Console.WriteLine("Ошибка! Заказ - {0}. Несовпадение типов для параметра {1} - \"{2}\"!", orderId, columnName, productIdStr);

            return result;
        }

        private int GetProductId(string productIdStr)
        {
            return Int32.Parse(productIdStr);
        }

        private bool ReportIfAmountIncorrect(int orderId, string amountString, string columnName)
        {
            try
            {
                GetAmountFromString(amountString);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка! Заказ - {0}. Параметр {1} некорректен - \"{2}\"!", orderId, columnName, amountString);
                return true;
            }
        }

        private float GetAmountFromString(string amount)
        {
            return float.Parse(amount);
        }

        private bool ReportIfDateIncorrect(int orderId, string dateTime, string columnName)
        {
            try
            {
                GetDateTimeFromString(dateTime);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка! Заказ - {0}. Параметр {1} некорректен - \"{2}\"!", orderId, columnName, dateTime);
                return true;
            }
        }

        private DateTime GetDateTimeFromString(string dateTime)
        {
            var dateAndTime = dateTime.Split('T');
            var date = dateAndTime[0].Split('.');
            var time = dateAndTime[1].Split(':');

            return new DateTime(Int32.Parse(date[2]), Int32.Parse(date[1]), Int32.Parse(date[0]),
                Int32.Parse(time[0]), Int32.Parse(time[1]), Int32.Parse(time[2]));
        }

        private bool ReportIfEmpty(int lineNumber, string param, string paramName) // Если пусто - выведет ошибку в консоль и вернет true
        {
            if (ParamIsEmpty(param))
            {
                Console.WriteLine("Ошибка! Заказ - {0}. Параметр {1} не содержит данных!", lineNumber, paramName);
                return true;
            }
            return false;
        }

        private bool ParamIsEmpty(string param)
        {
            return param.Equals("");
        }
    }
}
